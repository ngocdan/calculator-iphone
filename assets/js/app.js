// get number element
const getNumberElement = (name) => {
    return document.querySelector(name);
};

const number0El = getNumberElement(".number-0");
const number1El = getNumberElement(".number-1");
const number2El = getNumberElement(".number-2");
const number3El = getNumberElement(".number-3");
const number4El = getNumberElement(".number-4");
const number5El = getNumberElement(".number-5");
const number6El = getNumberElement(".number-6");
const number7El = getNumberElement(".number-7");
const number8El = getNumberElement(".number-8");
const number9El = getNumberElement(".number-9");

const divisionEl = getNumberElement(".division");
const multiplicationEl = getNumberElement(".multiplication");
const additionEl = getNumberElement(".addition");
const subtractionEl = getNumberElement(".subtraction");
const acEl = getNumberElement(".ac");
const pmEl = getNumberElement(".pm");
const percentEl = getNumberElement(".percent");
const decimalEl = getNumberElement(".decimal");
const equalEl = getNumberElement(".equal");
const valueEl = getNumberElement(".calculation-result");

const numberElArr = [
    number0El,
    number1El,
    number2El,
    number3El,
    number4El,
    number5El,
    number6El,
    number7El,
    number8El,
    number9El,
];

// variables
let valueStrInMemory = null;
let operatorInMemory = null;

// function
const getValueAsStr = () => valueEl.textContent.split(",").join("");

const getValueAsNum = () => {
    return parseFloat(getValueAsStr());
};
const setStrAsValue = (valueStr) => {
    if (valueStr[valueStr.length - 1] === ".") {
        valueEl.textContent += ".";
        return;
    }
    const [wholeNumStr, decimalStr] = valueStr.split(".");

    if (decimalStr) {
        valueEl.textContent =
            parseFloat(wholeNumStr).toLocaleString() + "." + decimalStr;
    } else {
        valueEl.textContent = parseFloat(wholeNumStr).toLocaleString();
    }
};

const handleNumberClick = (numStr) => {
    const currentValueStr = getValueAsStr();
    if (currentValueStr === "0") {
        // valueEl.textContent = numStr;
        setStrAsValue(numStr);
    } else {
        setStrAsValue(currentValueStr + numStr);
        // valueEl.textContent = parseFloat(
        //     currentValueStr + numStr
        // ).toLocaleString();
    }
};

const getResultOfOperationAsStr = () => {
    const currentValueNum = getValueAsNum();
    const valueNumInMemory = parseFloat(valueStrInMemory);

    let newValueNum;
    if (operatorInMemory === "addition") {
        newValueNum = valueNumInMemory + currentValueNum;
    } else if (operatorInMemory === "subtraction") {
        newValueNum = valueNumInMemory - currentValueNum;
    } else if (operatorInMemory === "multiplication") {
        newValueNum = valueNumInMemory * currentValueNum;
    } else if (operatorInMemory === "division") {
        newValueNum = valueNumInMemory / currentValueNum;
    }
    return newValueNum.toString();
};

const handleOperationClick = (operation) => {
    const currentValueStr = getValueAsStr();
    if (!valueStrInMemory) {
        valueStrInMemory = currentValueStr;
        operatorInMemory = operation;
        setStrAsValue("0");
        return;
    }

    valueStrInMemory = getResultOfOperationAsStr();
    operatorInMemory = operation;
    setStrAsValue("0");
};

acEl.addEventListener("click", () => {
    setStrAsValue("0");
    valueStrInMemory = null;
    operatorInMemory = null;
});
pmEl.addEventListener("click", () => {
    const currentValueNum = getValueAsNum();
    const currentValueStr = getValueAsStr();
    if (currentValueStr === "-0") {
        setStrAsValue("0");
        return;
    }
    if (currentValueNum >= 0) {
        setStrAsValue("-" + currentValueStr);
    } else {
        setStrAsValue(currentValueStr.substring(1));
    }
});
percentEl.addEventListener("click", () => {
    const currentValueNum = getValueAsNum();
    const newValueNum = currentValueNum / 100;
    setStrAsValue(newValueNum.toString());
    valueStrInMemory = null;
    operatorInMemory = null;
});

// Add event listeners to operators
additionEl.addEventListener("click", () => {
    handleOperationClick("addition");
});

subtractionEl.addEventListener("click", () => {
    handleOperationClick("subtraction");
});
multiplicationEl.addEventListener("click", () => {
    handleOperationClick("multiplication");
});
divisionEl.addEventListener("click", () => {
    handleOperationClick("division");
});
equalEl.addEventListener("click", () => {
    if (valueStrInMemory) {
        setStrAsValue(getResultOfOperationAsStr());
        valueStrInMemory = null;
        operatorInMemory = null;
    }
});

//Add event listeners to numbers and decimal
for (let i = 0; i < numberElArr.length; i++) {
    const numberEl = numberElArr[i];
    numberEl.addEventListener("click", () => {
        handleNumberClick(i.toString());
    });
}

decimalEl.addEventListener("click", () => {
    const currentValueStr = getValueAsStr();
    if (!currentValueStr.includes(".")) {
        // valueEl.textContent = currentValueStr + ".";
        setStrAsValue(currentValueStr + ".");
    }
});
// 1.09.47
